import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Асинхронные методы',
      home: Scaffold(
        body: MyHomePage(),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyHomePageState();
}

/*
 * Создадим класс песочные часы, в которых есть 100 песчинок,
 * и когда они в верхней части, значение sand=100, когда все
 * песчинки в нижней части — тогда sand=0
 */

class MyHomePageState extends State {
  // 2. объявление объекта clock класса SandGlass
  SandGlass clock = SandGlass();

  // 5. переопределение метода initState для запуска часов
  @override
  void initState() {
    super.initState();

    clock.tick();
  }

  /*
  * 7. Асинхронный метод вызывает перестроение виджета MyApp
  * после задержки в 1 секунду
  */
  _reDrawWidget() async {
    if (clock.time() == 0) return;
    await new Future.delayed(
      Duration(seconds: 1),
    );
    setState(() {
      print('_reDrawWidget()');
    });
  }

  @override
  Widget build(BuildContext context) {
    _reDrawWidget();

    return Center(
      // 3. вывести состояние часов методом time()
      //child: Text('Start'),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Time is ${clock.time()}'),
          // 8. добавлен вывод метода для понимания
          //Text('${_reDrawWidget()}'),
        ],
      ),
    );
  }
}

// 1. создать класс песочных часов
class SandGlass {
  int _sand = 0;

  time() {
    return _sand;
  }

  // 4. асинхронный метод
  tick() async {
    // 6. будем уменьшать значение песочных часов
    _sand = 100;

    print('Start: tick'); // вывод в консоль

    while (_sand > 0) {
      print('tick: $_sand');
      _sand--;

      await new Future.delayed(
        Duration(milliseconds: 100),
      );
    }

    print('End: tick');
  }
}
